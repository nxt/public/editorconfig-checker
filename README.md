# DEPRECATED

This template is deprecated.
We prefer [MegaLinter](https://megalinter.io), where editorconfig-checker is built-in.
Consult the [CI component template](https://gitlab.com/nxt/public/gitlab-ci-templates) for MegaLinter.

# GitLab CI/CD template for editorconfig-checker

This is a GitLab CI/CD template for editorconfig-checker.
It is kept up-to-date automatically by Renovate.

## Usage

```yaml
# .gitlab-ci.yml

stages:
  - lint

include:
  - project: 'nxt/public/editorconfig-checker'
    file: '/templates/gitlab-ci/template.yml'

    # Optional
    inputs:
      stage: lint
```

### Overwrite default configuration

The default configuration can be overwritten by adding a `editorconfig-checker` section to your `.gitlab-ci.yml` file,
and then setting the desired configuration.

E.g. to run in merge requests pipelines and main branch:

```yaml
stages:
  - lint

include:
  - project: 'nxt/public/editorconfig-checker'
    file: '/templates/gitlab-ci/template.yml'
    inputs:
      stage: lint

editorconfig-checker:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
```
